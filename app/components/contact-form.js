import Ember from 'ember';

export default Ember.Component.extend({
  contact: null,

  store: Ember.inject.service(),

  init() {
    this._super(...arguments);

    this.errorMessages = Ember.A();
    this.emailsToDelete = Ember.A();
    this.phonesToDelete = Ember.A();
  },

  actions: {
    save(event) {
      event.preventDefault();

      this.get('errorMessages').clear();

      let contact = this.get('contact');

      if (Ember.isEmpty(contact.get('name'))) {
        this.get('errorMessages').addObject('Name is required');
        return;
      }

      contact.save().then(() => {
        this.get('emailsToDelete').forEach((email) => {
          email.save();
        });

        contact.get('emailAddresses').forEach((email, index) => {
          email.save().catch(() => {
            this.get('errorMessages').addObject(`There was an error saving Email ${index + 1}. Please try again.`);
          });
        });

        this.get('phonesToDelete').forEach((phone) => {
          phone.save();
        });

        contact.get('phoneNumbers').forEach((phone, index) => {
          phone.save().catch(() => {
            this.get('errorMessages').addObject(`There was an error saving Phone ${index + 1}. Please try again.`);
          });
        });
      }).catch(() => {
        this.get('errorMessages').addObject('There was an error saving this contact. Please try again.');
      });
    },

    addPhone() {
      let contact = this.get('contact');
      let phoneNumber = this.get('store').createRecord('phone-number', {contactId: contact.get('id')});

      contact.get('phoneNumbers').pushObject(phoneNumber);
    },

    addEmail() {
      let contact = this.get('contact');
      let emailAddress = this.get('store').createRecord('email-address', {contactId: contact.get('id')});

      contact.get('emailAddresses').pushObject(emailAddress);
    },

    removeEmail(email) {
      this.get('contact.emailAddresses').removeObject(email);
      email.deleteRecord();
      this.get('emailsToDelete').pushObject(email);
    },

    removePhone(phone) {
      this.get('contact.phoneNumbers').removeObject(phone);
      phone.deleteRecord();
      this.get('phonesToDelete').pushObject(phone);
    }
  }
});
