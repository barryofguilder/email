import Ember from 'ember';

export default Ember.Controller.extend({
  contactsSort: ['name:asc'],
  sortedModel: Ember.computed.sort('model', 'contactsSort')
});
