import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { hasMany } from 'ember-data/relationships';
import computed from 'ember-computed';

export default Model.extend({
  name: attr('string'),
  emailAddresses: hasMany('email-address'),
  phoneNumbers: hasMany('phone-number'),

  primaryEmail: computed.alias('emailAddresses.firstObject.address'),
  additionalEmailCount: computed('emailAddresses.length', function() {
    var emailCount = this.get('emailAddresses.length');

    return emailCount === 0 ? 0 : emailCount - 1;
  }),

  primaryPhone: computed.alias('phoneNumbers.firstObject.number'),
  additionalPhoneCount: computed('phoneNumbers.length', function() {
    var phoneCount = this.get('phoneNumbers.length');

    return phoneCount === 0 ? 0 : phoneCount - 1;
  })
});
