import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
  number() { return faker.phone.phoneNumberFormat(); }
});
