import { test } from 'qunit';
import moduleForAcceptance from 'email/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | contacts index');

test('visiting user contacts route displays all contacts', function(assert) {
  server.createList('contact', 5);

  visit('/contacts');

  andThen(function() {
    assert.equal(find('.test-contact').length, 5, 'All contacts display');
  });
});

test('user contacts list is sorted by name', function(assert) {
  server.create('contact', {name: 'John Armstrong'});
  server.create('contact', {name: 'John Doe'});
  server.create('contact', {name: 'Jane Doe'});

  visit('/contacts');

  andThen(function() {
    assert.contains('.test-contact:eq(0) .test-contact-name', 'Jane Doe', 'Contact list is sorted alphabetically');
    assert.contains('.test-contact:eq(1) .test-contact-name', 'John Armstrong', 'Contact list is sorted alphabetically');
    assert.contains('.test-contact:eq(2) .test-contact-name', 'John Doe', 'Contact list is sorted alphabetically');
  });
});

test('displays info for a contact in each row', function(assert) {
  let emailAddresses = server.createList('email-address', 3);
  emailAddresses[0].address = 'smith@example.com';
  let phoneNumbers = server.createList('phone-number', 2);
  phoneNumbers[0].number = '+1 555-555-5252';
  server.create('contact', {
    name: 'Ann Smith',
    emailAddresses: emailAddresses,
    phoneNumbers: phoneNumbers
  });

  visit('/contacts');

  andThen(function() {
    assert.contains('.test-contact:eq(0) .test-contact-name', 'Ann Smith', 'Contact row displays contact name');
    assert.contains('.test-contact:eq(0) .test-contact-primary-email', 'smith@example.com', 'Contact row displays contact primary email');
    assert.contains('.test-contact:eq(0) .test-contact-primary-email', '(+2)', 'Displays additional email count');
    assert.contains('.test-contact:eq(0) .test-contact-primary-phone', '+1 555-555-5252', 'Contact row displays primary phone');
    assert.contains('.test-contact:eq(0) .test-contact-primary-phone', '(+1)', 'Displays additional phone count');
  });
});

// Write the tests
test('name links to edit for a contact', function(assert) {
  let contact = server.create('contact');

  visit('/contacts');
  click('.test-contact-name a:eq(0)');

  andThen(function() {
    assert.equal(currentURL(), `/contacts/${contact.id}`, 'Now on the contact edit page');
  });
});

test('primary email is a mailto: link', function(assert) {
  let emailAddress = server.create('email-address');
  server.create('contact', {emailAddresses: [emailAddress]});

  visit('/contacts');

  andThen(function() {
    assert.equal(find('.test-contact:eq(0) .test-contact-primary-email a').attr('href'), `mailto:${emailAddress.address}`, 'Primary email is a mailto: link');
  });
});

test('primary phone is a tel: link', function(assert) {
  let phoneNumber = server.create('phone-number');
  server.create('contact', {phoneNumbers: [phoneNumber]});

  visit('/contacts');

  andThen(function() {
    assert.equal(find('.test-contact:eq(0) .test-contact-primary-phone a').attr('href'), `tel:${phoneNumber.number}`, 'Primary phone is a tel: link');
  });
});
