import { test } from 'qunit';
import moduleForAcceptance from 'email/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | contacts edit');

test('Edit contact displays contact current info', function(assert) {
  let phoneNumber1 = server.create('phone-number', { number: '+15555555252' });
  let phoneNumber2 = server.create('phone-number', { number: '+15555550002' });
  let emailAddress1 = server.create('email-address', { address: 'smith@example.com' });
  let emailAddress2 = server.create('email-address', { address: 'smith@example.org' });
  let emailAddress3 = server.create('email-address', { address: 'asmith@example.edu' });
  let contact = server.create('contact', {
    name: 'Ann Smith',
    phoneNumbers: [phoneNumber1, phoneNumber2],
    emailAddresses: [emailAddress1, emailAddress2, emailAddress3]
  });

  visitEditContact(contact);

  andThen(function() {
    assert.contains('.test-contact-name input', 'Ann Smith', 'Contact name is displayed');
    assert.contains('.test-contact-phone input', '+15555555252', 'Contact primary phone is displayed');
    assert.contains('.test-contact-phone input', '+15555550002', 'Contact alternate phone is displayed');
    assert.contains('.test-contact-email input', 'smith@example.com', 'Contact primary email is displayed');
    assert.contains('.test-contact-email input', 'smith@example.org', 'Contact alternate email is displayed');
    assert.contains('.test-contact-email input', 'asmith@example.edu', 'Contact work email is displayed');
  });
});

test('Edit contact allows editing contact info', function(assert) {
  let phoneNumber1 = server.create('phone-number', { number: '+15555555252' });
  let emailAddress1 = server.create('email-address', { address: 'smith@example.com' });
  let contact = server.create('contact', {
    name: 'Ann Smith',
    phoneNumbers: [phoneNumber1],
    emailAddresses: [emailAddress1]
  });

  visitEditContact(contact);

  fillIn('.test-contact-name input', 'Bob Saget');
  fillIn('.test-contact-phone:eq(0) input', '+14444442222');
  fillIn('.test-contact-email:eq(0) input', 'bsaget@example.com');
  click('.test-save');

  andThen(function() {
    let updatedContact = server.db.contacts[0];
    assert.equal(updatedContact.name, 'Bob Saget', 'Contact name is updated');

    let phone1 = server.db.phoneNumbers[0];
    assert.equal(phone1.number, '+14444442222', 'Primary phone is updated');

    let email1 = server.db.emailAddresses[0];
    assert.equal(email1.address, 'bsaget@example.com', 'Primary email is updated');
  });
});

test('Edit contact handles server error when saving contact', function(assert) {
  let phoneNumber1 = server.create('phone-number', { number: '+15555555252' });
  let emailAddress1 = server.create('email-address', { address: 'smith@example.com' });
  let contact = server.create('contact', {
    name: 'Ann Smith',
    phoneNumbers: [phoneNumber1],
    emailAddresses: [emailAddress1]
  });

  server.patch('/contacts/:id', {errors: ['there was an error']}, 500);

  visitEditContact(contact);
  fillIn('.test-contact-name input', 'Bob Saget');
  fillIn('.test-contact-phone:eq(0) input', '+14444442222');
  fillIn('.test-contact-email:eq(0) input', 'bsaget@example.com');
  click('.test-save');

  andThen(function() {
    let updatedContact = server.db.contacts[0];
    assert.equal(updatedContact.name, 'Ann Smith', 'Contact name is NOT updated');

    let phone1 = server.db.phoneNumbers[0];
    assert.equal(phone1.number, '+15555555252', 'Primary phone is NOT updated');

    let email1 = server.db.emailAddresses[0];
    assert.equal(email1.address, 'smith@example.com', 'Primary email is NOT updated');

    assert.contains('.alert-danger', 'There was an error saving this contact. Please try again.');
  });
});

test('Edit contact handles server error when saving contact phone, saving all related models other than the erroring one', function(assert) {
  let phoneNumber1 = server.create('phone-number', { number: '+15555555252' });
  let phoneNumber2 = server.create('phone-number', { number: '+15555550002' });
  let emailAddress1 = server.create('email-address', { address: 'smith@example.com' });
  let contact = server.create('contact', {
    name: 'Ann Smith',
    phoneNumbers: [phoneNumber1, phoneNumber2],
    emailAddresses: [emailAddress1]
  });

  server.patch('/phone-numbers/1', {errors: ['there was an error']}, 500);

  visitEditContact(contact);
  fillIn('.test-contact-name input', 'Bob Saget');
  fillIn('.test-contact-phone:eq(0) input', '+14444442222');
  fillIn('.test-contact-phone:eq(1) input', '+14444443333');
  fillIn('.test-contact-email:eq(0) input', 'smithyyyy@example.com');
  click('.test-save');

  andThen(function() {
    let updatedContact = server.db.contacts[0];
    assert.equal(updatedContact.name, 'Bob Saget', 'Contact name is updated');

    let phone1 = server.db.phoneNumbers[0];
    assert.equal(phone1.number, '+15555555252', 'Primary phone is NOT updated');

    let phone2 = server.db.phoneNumbers[1];
    assert.equal(phone2.number, '+14444443333', 'Alternate phone is updated');

    let email1 = server.db.emailAddresses[0];
    assert.equal(email1.address, 'smithyyyy@example.com', 'Primary email is updated');

    assert.contains('.alert-danger', 'There was an error saving Phone 1. Please try again.');
  });
});

test('Edit contact handles server error when saving contact email, saving all related models other than the erroring one', function(assert) {
  let phoneNumber1 = server.create('phone-number', { number: '+15555555252' });
  let emailAddress1 = server.create('email-address', { address: 'smith@example.com' });
  let emailAddress2 = server.create('email-address', { address: 'smith@example.org' });
  let contact = server.create('contact', {
    name: 'Ann Smith',
    phoneNumbers: [phoneNumber1],
    emailAddresses: [emailAddress1, emailAddress2]
  });

  server.patch('/email-addresses/2', {errors: ['there was an error']}, 500);

  visitEditContact(contact);
  fillIn('.test-contact-name input', 'Bob Saget');
  fillIn('.test-contact-phone:eq(0) input', '+14444442222');
  fillIn('.test-contact-email:eq(0) input', 'smithyyyy@example.com');
  fillIn('.test-contact-email:eq(1) input', 'sssssmith@email.com');
  click('.test-save');

  andThen(function() {
    let updatedContact = server.db.contacts[0];
    assert.equal(updatedContact.name, 'Bob Saget', 'Contact name is updated');

    let phone1 = server.db.phoneNumbers[0];
    assert.equal(phone1.number, '+14444442222', 'Primary phone is updated');

    let email1 = server.db.emailAddresses[0];
    assert.equal(email1.address, 'smithyyyy@example.com', 'Primary email is updated');

    let email2 = server.db.emailAddresses[1];
    assert.equal(email2.address, 'smith@example.org', 'Alternate email is NOT updated');

    assert.contains('.alert-danger', 'There was an error saving Email 2. Please try again.');
  });
});

test('Edit contact does not allow empty name', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });

  visitEditContact(contact);
  fillIn('.test-contact-name input', '');
  click('.test-save');

  andThen(function() {
    let updatedContact = server.db.contacts[0];
    assert.equal(updatedContact.name, 'Ann Smith', 'Contact name is NOT updated');
    assert.contains('.alert-danger', 'Name is required');
  });

  fillIn('.test-contact-name input', 'Bob Saget');
  click('.test-save');

  andThen(function() {
    let updatedContact = server.db.contacts[0];
    assert.equal(updatedContact.name, 'Bob Saget', 'Contact name is updated');
  });
});

test('Edit contact allows adding a phone', function(assert) {
  let phoneNumber1 = server.create('phone-number', { number: '+15555555252' });
  let emailAddress1 = server.create('email-address', { address: 'smith@example.com' });
  let contact = server.create('contact', {
    name: 'Ann Smith',
    phoneNumbers: [phoneNumber1],
    emailAddresses: [emailAddress1]
  });

  visitEditContact(contact);
  click('.test-add-phone');
  fillIn('.test-contact-phone:eq(1) input', '+14444442222');
  click('.test-save');

  andThen(function() {
    let phoneNumbers = server.db.phoneNumbers;
    assert.equal(phoneNumbers.length, 2, 'A new phone was added');

    let phone = phoneNumbers[phoneNumbers.length - 1];
    assert.equal(phone.number, '+14444442222', 'New phone contains the correct value');
  });
});

test('Edit contact allows adding an email', function(assert) {
  let phoneNumber1 = server.create('phone-number', { number: '+15555555252' });
  let emailAddress1 = server.create('email-address', { address: 'smith@example.com' });
  let contact = server.create('contact', {
    name: 'Ann Smith',
    phoneNumbers: [phoneNumber1],
    emailAddresses: [emailAddress1]
  });

  visitEditContact(contact);
  click('.test-add-email');
  fillIn('.test-contact-email:eq(1) input', 'smith@example.org');
  click('.test-save');

  andThen(function() {
    let emailAddresses = server.db.emailAddresses;
    assert.equal(emailAddresses.length, 2, 'A new email was added');

    let email = emailAddresses[emailAddresses.length - 1];
    assert.equal(email.address, 'smith@example.org', 'New email contains the correct value');
  });
});

test('Edit contact allows adding multiple phones and/or emails at the same time', function(assert) {
  let phoneNumber1 = server.create('phone-number', { number: '+15555555252' });
  let emailAddress1 = server.create('email-address', { address: 'smith@example.com' });
  let contact = server.create('contact', {
    name: 'Ann Smith',
    phoneNumbers: [phoneNumber1],
    emailAddresses: [emailAddress1]
  });

  visitEditContact(contact);
  click('.test-add-phone');
  fillIn('.test-contact-phone:eq(1) input', '+14444442222');
  click('.test-add-phone');
  fillIn('.test-contact-phone:eq(2) input', '+15555550002');
  click('.test-add-email');
  fillIn('.test-contact-email:eq(1) input', 'smith@example.org');
  click('.test-add-email');
  fillIn('.test-contact-email:eq(2) input', 'asmith@example.edu');
  click('.test-save');

  andThen(function() {
    let phoneNumbers = server.db.phoneNumbers;
    assert.equal(phoneNumbers.length, 3, 'The new phones were added');

    let phone1 = phoneNumbers[phoneNumbers.length - 2];
    assert.equal(phone1.number, '+14444442222', 'New phone 1 contains the correct value');

    let phone2 = phoneNumbers[phoneNumbers.length - 1];
    assert.equal(phone2.number, '+15555550002', 'New phone 2 contains the correct value');

    let emailAddresses = server.db.emailAddresses;
    assert.equal(emailAddresses.length, 3, 'The new emails were added');

    let email1 = emailAddresses[emailAddresses.length - 2];
    assert.equal(email1.address, 'smith@example.org', 'New email 1 contains the correct value');

    let email2 = emailAddresses[emailAddresses.length - 1];
    assert.equal(email2.address, 'asmith@example.edu', 'New email 2 contains the correct value');
  });
});

test('Edit contact allows removing emails', function(assert) {
  let phoneNumber1 = server.create('phone-number', { number: '+15555555252' });
  let emailAddress1 = server.create('email-address', { address: 'smith@example.com' });
  let contact = server.create('contact', {
    name: 'Ann Smith',
    phoneNumbers: [phoneNumber1],
    emailAddresses: [emailAddress1]
  });

  visitEditContact(contact);
  click('.test-remove-email:eq(0)');
  click('.test-save');

  andThen(function() {
    let emailAddresses = server.db.emailAddresses;
    assert.equal(emailAddresses.length, 0, 'The email was removed');
  });
});

test('Edit contact allows removing phones', function(assert) {
  let phoneNumber1 = server.create('phone-number', { number: '+15555555252' });
  let emailAddress1 = server.create('email-address', { address: 'smith@example.com' });
  let contact = server.create('contact', {
    name: 'Ann Smith',
    phoneNumbers: [phoneNumber1],
    emailAddresses: [emailAddress1]
  });

  visitEditContact(contact);
  click('.test-remove-phone:eq(0)');
  click('.test-save');

  andThen(function() {
    let phoneNumbers = server.db.phoneNumbers;
    assert.equal(phoneNumbers.length, 0, 'The phone was removed');
  });
});

// contact is a mirage factory
function visitEditContact(contact) {
  visit(`/contacts/${contact.id}`);
}
