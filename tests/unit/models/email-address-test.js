import { moduleForModel, test } from 'ember-qunit';

moduleForModel('email-address', 'Unit | Model | email address', {
  // Specify the other units that are required for this test.
  needs: ['model:contact']
});

test('it exists', function(assert) {
  let model = this.subject();
  // let store = this.store();
  assert.ok(!!model);
});
